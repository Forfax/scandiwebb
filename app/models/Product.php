<?php
require_once("Entity.php");

class Product extends Entity{
    public function __construct(){
      $this->tableName = "products";
      $this->db = new Database();
    }

    // Get All Products
    public function get(){
      $this->db->query("
      SELECT
      p.product_id,
      p.product_name Name,
      p.product_sku SKU,
      p.product_price Price,
      ct.category_name as Category,
      case when ct.category_id = 2 then
          c.size 
      end as Size,

      case when ct.category_id = 1 then
          b.weight 
      end as Weight,

      case when ct.category_id = 3 then
          f.length 
      end as Length,

      case when ct.category_id = 3 then
          f.width
      end as Width,

      case when ct.category_id = 3 then
          f.height
      end as Height

      FROM products p
      inner join categories ct on p.category_id = ct.category_id
      left join book b on b.book_id = p.product_id
      left join furniture f on f.furniture_id = p.product_id
      left join cd c on c.cd_id = p.product_id
      order by p.product_price
      ");

      $results = $this->db->resultset();
     // print_r($results);
      return $results;
    }

    // Add Product
    public function add($data){
      // Prepare Query
      $this->db->query('INSERT INTO products (product_name, product_price, product_sku, category_id) 
      VALUES (:product_name, :product_price, :product_sku, :category_id)');

      // Bind Values
      $this->db->bind(':product_name', $data['product_name']);
      $this->db->bind(':product_price', $data['product_price']);
      $this->db->bind(':product_sku', $data['product_sku']);
      $this->db->bind(':category_id', $data['category_id']);
      
      //Execute
      if($this->db->execute()){
        if (!$this->addToCategory($data['additional_data'], $data['category_id'])) return false;
        return true;
      } else {
        return false;
      }
    }

    // Delete Product
    public function delete($id){
     
      $this->db->query("DELETE FROM `products` WHERE product_id = $id");
      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }

    private function addToCategory($data, $categoryId) {
      $data = array_filter($data);
      $categoryName = CategoryExtension::getCategoryName($categoryId);
      $keys = array_keys($data);
      $names = implode(', ', $keys);
      $values = '';
      $productId = $this->db->lastInsertId();
      $tableIdName = $categoryName . "_id";
      
      for ($i = 0; $i < sizeof($keys); $i++) {
        $values .= ":$keys[$i], ";
      }
      
      $names .= ", " . $tableIdName;
      $values .= " :" . $tableIdName;
      $this->db->query("INSERT INTO $categoryName ($names) VALUES ($values)");
      
      foreach ($data as $key => $value) {
        $this->db->bind(":$key", $value);
      }
      $this->db->bind(":$tableIdName", $productId);

      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }
  }