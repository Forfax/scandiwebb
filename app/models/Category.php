<?php

require_once("Entity.php");

class Category extends Entity {
    
    public function __construct(){
      $this->db = new Database;
      $this->tableName = "categories";
    }
    // Get All Products
    public function get(){
      $this->db->query("SELECT * FROM categories");

      $results = $this->db->resultset();

      return $results;
    }
}