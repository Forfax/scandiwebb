<?php
require_once("Entity.php");

abstract class ProductEntity extends Entity {
    abstract public function add($data);
    abstract public function delete($id);
    abstract public function addToCategory($id, $categoryId);
}