<?php
  class Products extends Controller{
    public function __construct(){
      // Load Models
      $this->productModel = $this->model('Product');
      $this->categoryModel = $this->model('Category');
    }

    // Load All Products
    public function index(){
      $products = $this->productModel->get();
      $data = [
        'products' => $products
      ];
      $this->view('products/index', $data);
    }

    // Add Product
    public function add(){
      $data = [
        'product_name' => '',
        'product_sku' => '',
        'product_price' => '',
        'category_id' => '',
        'categories' => $this->categoryModel->get()
      ];

      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // Sanitize POST
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
        $data = [
          'Name_err' => '',
          'Price_err' => '',
          'Producttype_err' => '',
          'typeval_err' => '',
          'SKU_err' => '',
          'product_name' => array_key_exists('product_name', $_POST) ? trim($_POST['product_name']) : "",
          'product_sku' => array_key_exists('product_sku', $_POST) ? trim($_POST['product_sku']) : "",
          'product_price' => array_key_exists('product_price', $_POST) ? trim($_POST['product_price']) : "",
          'category_id' => array_key_exists('category_id', $_POST) ? trim($_POST['category_id']) : "",
          'categories' => $data['categories'],
          'additional_data' => [
            'weight' => trim($_POST['weight']??''),
            'size' => trim($_POST['size']??''),
            'length' => trim($_POST['length']??''),
            'height' => trim($_POST['height']??''),
            'width' => trim($_POST['width']??'')
          ]
        ];

        if(empty($data['product_name'])){
          $data['Name_err'] = 'Please enter name';
        }

        if(empty($data['product_price'])){
          $data['Price_err'] = 'Please enter the Price';
        } elseif(!is_numeric($data['product_price'])) {
          $data['Price_err'] = 'Enter the valid Price';
        }

        if(empty($data['product_sku'])){
          $data['SKU_err'] = 'Please enter SKU';
        }

        if(empty($data['category_id'])){
          $data['Producttype_err'] = 'Please enter Category';
        }

        
        // Make sure there are no errors
        if(empty($data['Name_err']) && empty($data['Price_err']) && empty($data['Producttype_err']) && empty($data['typeval_err']) && empty($data['SKU_err']) && empty($data['Producttype_err'])) {
          // Validation passed

          //Execute
          if(!$this->productModel->add($data)){
            throw new Exception("Something went wrong");
          }

          redirect('products');
          return;
        }
      } 
      
      $this->view('products/add', $data);
    }

    // Delete Post
    public function delete(){
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // Sanitize POST
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        //print_r($_POST['records']);
        if(isset($_POST['records'])) {
          foreach($_POST['records'] as $recordId) {
            // print_r($recordId);
            if($this->productModel->delete($recordId))
            {
              redirect('products');
            }
            else 
            {
              die('Something went wrong');
            }
          }
        } else {
          redirect('products');
        }     
      }
    }
  }