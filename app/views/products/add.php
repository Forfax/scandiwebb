<?php require APPROOT . '/views/inc/header.php'; ?>
    <div class="col-md-6 btn-config">
        <a class="btn btn-primary pull-right" id="savebtn">Save </a>  
        <a id="deleteButton" class="btn btn-danger dlt" href="<?php echo URLROOT; ?>/products">Cancel</a>
    </div>
</nav>

<div class="secondary-title">
    <h1>Add Post</h1>
</div>

<div class="card card-body bg-light mt-5">
    <form   class="add_product_form" 
            id="product_form" 
            action="<?php echo URLROOT; ?>/products/add" 
            method="post">
        <div class="inputs_container">
            <div class="form-group">
                <label class="label">Name:</label>
                <input 
                    type="text" 
                    name="product_name"
                    id="name"
                    class="input form-control form-control-lg 
                        <?php echo (!empty($data['Name_err'])) ? 'is-invalid' : ''; ?>" 
                    value="<?php echo $data['product_name']; ?>"
                    placeholder="Add a name...">
                <!-- <span class="invalid-feedback"><?php echo $data['Name_err']; ?></span> -->
            </div>

            <div class="form-group">
                <label class="label">Price:</label>
                <input
                required
                type="number"
                name="product_price"
                id="price"
                class="input form-control form-control-lg 
                    <?php echo (!empty($data['Price_err'])) ? 'is-invalid' : ''; ?>" 
                value="<?php echo $data['product_price']; ?>" 
                placeholder="Add a price...">
                <!-- <span class="invalid-feedback">Set Price</span> -->
            </div>
            
            <div class="form-group">
                <label class="label">SKU:</label>
                <input type="text" 
                 
                name="product_sku" 
                id="sku" 
                class="input form-control form-control-lg 
                    <?php echo (!empty($data['SKU_err'])) ? 'is-invalid' : ''; ?>" 
                value="<?php echo $data['product_sku']; ?>" placeholder="Add a SKU...">
                <!-- <span class="invalid-feedback"><?php echo $data['SKU_err']; ?></span> -->
            </div>


            <div class="form-group">
                <label class="label">Select Category:</label>
                <select required name="category_id" id="productType" class="input form-select" >
                    <option class="option-placeholder" disabled selected>
                        Select category...
                    </option>
                    
                    <?php
                    foreach ($data['categories'] as $category) {
                    ?>
                        <option value="<?php echo $category->category_id; ?>">
                            <?php echo $category->category_name; ?>
                        </option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <!-- <span class="invalid-feedback"><?php echo $data['Producttype_err']; ?></span> -->
            <div class="added_inputs mb-5"></div>
        </div>
    </form>
</div>

    <script type="text/javascript" src="<?php echo URLROOT; ?>/javascript/product.js" />
    <script type="text/javascript" src="<?php echo URLROOT; ?>/javascript/index.js" />
<?php require APPROOT . '/views/inc/footer.php'; ?>
