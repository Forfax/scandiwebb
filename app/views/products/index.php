<?php require APPROOT . '/views/inc/header.php'; ?>
  <div class="col-md-6 btn-config">
      <a  class="btn btn-primary pull-right" 
          href="<?php echo URLROOT; ?>/products/add">
          <i class="fa fa-pencil" aria-hidden="true"></i> 
          Add
      </a>  
      <a id="deleteButton" class="btn btn-danger dlt">Mass Delete</a>
    </div> 
</nav>

  <div class="row mb-3 section">
    <h1>Product List</h1>
  </div>

  <section class="hero">
    <form action="<?php echo URLROOT; ?>/products/delete" 
            method="post" 
            id="deleteForm"
            class="grid">
        <?php foreach($data['products'] as $product) : ?>
          <div class="item-card">
              <input type="checkbox" class="delete-checkbox" value="<?php echo $product->product_id; ?>" name="records[]" />
                <?php 
                foreach($product as $key => $value) {
                  if($value != "" && !in_array($key, ["product_id", "Length", "Width", "Height"])) {
                  ?>
                    <p class="card-text"><?php echo $value; ?></p>
                  <?php
                  }
                }
                if($product->Category == 'Furniture') {
                  ?>
                    <p class="card-text"><?php echo $product->Width." x ".$product->Height." x ".$product->Length; ?></p> 
                  <?php
                }
                ?>
          </div>
        <?php endforeach; ?>
        
      </form>
  </section>

  <script type="text/javascript" src="<?php echo URLROOT; ?>/javascript/index.js" />
<?php require APPROOT . '/views/inc/footer.php'; ?>
