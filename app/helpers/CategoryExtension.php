<?php

class CategoryExtension {
    public static function getCategoryName($id) {
        switch ($id) {
            case 1:
              return 'book';
    
            case 2:
              return 'cd';
    
            case 3:
              return 'furniture';
            
            default:
            throw new ErrorException('Unkown category id');
              break;
          }
    }
}