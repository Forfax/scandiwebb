const categoryInputs = ["Weight", "Size", ["Length", "Width", "Height"]];
const selectEl = document.querySelector("#productType");
const addedInputsContainer = document.querySelector(
    ".inputs_container > .added_inputs"
);

const selectSave = document.querySelector("#savebtn");
selectSave.addEventListener("click", submitForm);



function inputErrorController(inputEl, addError) {
    const prevErrorEl = inputEl.parentNode.querySelector('.invalid-feedback');

    if (prevErrorEl) 
        prevErrorEl.remove()
    
    if (addError) {
        inputEl.classList.add('is-invalid');
        
        const errorEl = document.createElement('span');
        errorEl.classList.add('invalid-feedback');
        errorEl.textContent = `set ${inputEl.id}`;
        inputEl.parentNode.insertBefore(errorEl, inputEl.nextSibling);
    } else {
        inputEl.classList.remove('is-invalid');
    }
}





function submitForm() {
    const formEl = document.querySelector('#product_form');
    const everyInputEl = formEl.querySelectorAll('input')
    const selectEl = document.querySelector("#productType");

    const data = Object.fromEntries(new FormData(formEl).entries());
    const ifEmpty = Object.values(data).find(value => value.length === 0);

    console.log(Object.values(data));
    console.log(ifEmpty)

    if (ifEmpty !== '' && !(selectEl.selectedIndex <= 0)) {
        formEl.submit();
        console.log('submiting');
    }
    else {
        inputErrorController(selectEl, selectEl.selectedIndex <= 0)
        everyInputEl.forEach((inputEl) => {
            if (inputEl.value.length === 0) {
                inputErrorController(inputEl, true);
                return
            } else {
                inputErrorController(inputEl, false);
                return
            }
        })
    }

}

selectEl.addEventListener(
    "change",
    ({ target }) => {
        const id = target.value;
        addedInputsContainer.innerHTML = "";
        if (typeof categoryInputs[id - 1] === "object") {
            categoryInputs[id - 1].map((lableString, i) => {
                createInputElement(lableString);
            });
            return;
        }
        createInputElement(categoryInputs[id - 1]);
    },
    false
);


const createInputElement = (lableString) => {
    addedInputsContainer.insertAdjacentHTML("beforeend",
        `<div class="form-group">
            <label class="label">${lableString}:</label>
            <input  
                type="number"
                required
                name="${lableString.toLowerCase()}"
                id="${lableString.toLowerCase()}"
                class="input form-control form-control-lg" 
                placeholder="Add ${lableString}...">
        </div>`
    );
};

function validate(e) {
    console.log(e);
}