const selectEl = document.querySelector("#deleteButton");

selectEl.addEventListener("click", submitDeleteForm);

function submitDeleteForm() {
    document.querySelector("#deleteForm").submit();
}
